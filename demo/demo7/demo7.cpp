#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <vector>
#include <complex>
#include <string.h>

using namespace Eigen;
using namespace std;

int main(){
	
	string sep = "\n------------------------------------\n"	;
	//------------稀疏矩阵  储存格式输出--------------//
	
	Matrix<double,5,5, RowMajor> mat;
	
	mat << 0, 3, 0, 0, 0,
		  22, 0, 0, 0,17,
		   7, 5, 0, 1, 0,
		   0, 0, 0, 0, 0,
		   0, 0,14, 0, 8;
	
	SparseMatrix<double,RowMajor> S = mat.sparseView(1, 1e-20);
	
	cout << S << sep;
	cout  << " RowMajor " << "\n";
	int nnz = S.nonZeros();
	cout  << "S.nonZeros() = "<< nnz << "\n";
		
	double *S_value = S.valuePtr();
	int *S_InnerIndices = S.innerIndexPtr();
	int *S_OuterStarts = S.outerIndexPtr();
	
	cout  << "S.valuePtr:" <<" ";
	for(int i=0; i<S.nonZeros(); i++){
		
		cout<<S_value[i]<<",";
		
	}
			
	cout  << "\n"<< "S.innerIndexPtr:" << " ";
	
	for(int i=0; i<S.nonZeros(); i++){
		
		cout<<S_InnerIndices[i]<<",";
		
	}
	
	cout  << "\n"<< "S.S_OuterStarts:" << " ";
	
	for(int i=0; i< S.rows () +1; i++){
			
		cout<<S_OuterStarts[i]<<",";
		
	}
	
	cout << sep;
	//-------------------------------------//
	
	SparseMatrix<double,ColMajor> S2 = mat.sparseView(1, 1e-20);
	cout << S2 << sep;
	cout  << " ColMajor " << "\n";
	int nnz2 = S2.nonZeros();
	cout  << "S2.nonZeros() = "<< nnz2 << "\n";
		
	double *S2_value = S2.valuePtr();
	int *S2_InnerIndices = S2.innerIndexPtr();
	int *S2_OuterStarts = S2.outerIndexPtr();
	
	cout  << "S2.valuePtr:" <<" ";
	for(int i=0; i<S2.nonZeros(); i++){
		
		cout<<S2_value[i]<<",";
		
	}
			
	cout  << "\n"<< "S2.innerIndexPtr:" << " ";
	
	for(int i=0; i<S2.nonZeros(); i++){
		
		cout<<S2_InnerIndices[i]<<",";
		
	}
	
	cout  << "\n"<< "S2.S_OuterStarts:" << " ";
	
	for(int i=0; i< S2.cols () +1; i++){
			
		cout<<S2_OuterStarts[i]<<",";
		
	}
	
	cout << sep;
	
}
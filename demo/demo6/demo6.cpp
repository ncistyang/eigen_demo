#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <vector>
#include <complex>
#include <string.h>

using namespace Eigen;
using namespace std;


int main(){
		
	string sep = "\n------------------------------------\n"	;
		
	//-------insert  稀疏矩阵insert 赋值------------------//
	SparseMatrix<double> mat(5,5); 
	
	mat.insert(0,0) = 1;
	mat.insert(0,1) = 2;
	
	mat.makeCompressed();  //CCS  Compressed Column Storage 
	cout << "Here is the sparse matrix mat:\n"<<mat<<sep;
	
	//--------Triplet  稀疏矩阵三元vector赋值-------------//
	vector<Triplet<complex<double>>> vect;
	
	vect.push_back(Triplet<complex<double>>(0,0,{1,1}));
	vect.push_back(Triplet<complex<double>>(1,1,{2,2}));
	vect.push_back(Triplet<complex<double>>(2,2,{3,3}));
	
	SparseMatrix<complex<double>> mat2(3,3);
	mat2.setFromTriplets(vect.begin(), vect.end());
	cout  << "Here is the sparse matrix mat2:\n"<<mat2<<sep;
	
	//----------稀疏矩阵 密实矩阵转化赋值----------------//
	VectorXd vect1 = VectorXd::Ones(5);
	Matrix<double,5,5> mat3 = vect1.asDiagonal();
	
	SparseMatrix<double> S = mat3.sparseView(1, 1e-20);
	
	cout  << "Here is the sparse matrix S:\n"<<S<<sep;
	
	//------------复数稀疏矩阵 由密实矩阵构造--------------//

	complex<double> c{0,1};
	VectorXcd vect2 = VectorXd::Ones(4) +  c*VectorXd::Ones(4);
	
	Matrix<complex<double>,4,4> mat4 = vect2.asDiagonal();	
	SparseMatrix<complex<double>> S2 = mat4.sparseView(1, 1e-20);
	
	S2.makeCompressed();	
	
	cout  << "Here is the sparse matrix S2:\n"<< S2 << sep;
	
}
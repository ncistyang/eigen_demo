#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
//#include <cuComplex.h>
#include <string.h>
#include <complex>

using namespace Eigen;
using namespace std;

int main(){
	
	string sep = "\n----------------------------\n";
	
	SparseMatrix<complex<double>> sMat(2,2);
	
	sMat.insert(0,0) = {0,0};
	sMat.insert(0,1) = {1,1};
	sMat.insert(1,0) = {2,2};
	sMat.insert(1,1) = {3,3};	
	sMat.makeCompressed();
	cout << "Here is the Sparse complex matrix sMat:\n" << sMat << sep;
	
	MatrixXcd cuMat(2,2);
	cuMat.real() << 0,1,2,3;
	cuMat.imag() << 0,1,2,3;
	
	cout << "Here is the complex matrix cuMat: \n"<< cuMat <<sep;
	
	Matrix<complex<double>,2,2,RowMajor> cuMat2;
	cuMat2.real() << 0,1,2,3;
	cuMat2.imag() << 0,0,2,3;
	
	
	cout << "Here is the complex<double> matrix cuMat2: \n"<< cuMat2 <<sep;
	
	complex<double> *data = (complex<double>*)cuMat2.data();
	for(int i = 0;i< cuMat2.size();i++){
		
		cout << "("<<data[i].real() << ","<<data[i].imag()<<") ";
		
	}
	cout << sep;
		
	Map<MatrixXcd> map((complex<double>*)data,2,2);
	
	cout << "Here is map:\n" << map<< sep;
	
	SparseMatrix< complex<double>, RowMajor> S = map.sparseView(1, 1e-20); //过滤小于1*1e20数据
	
	S.makeCompressed();
	
	cout << "Here is S:\n" << S<< sep;
	
}

# include <iostream>
# include <Eigen/Dense>

using namespace std;
using namespace Eigen;

int main(){
	
	Matrix<double, 2, 3, ColMajor> m;
	m << 1,2,3,4,5,6;
	
	cout<<"-----------\n"<< "m:\n" << m <<endl;
	
	double *array = (double *)malloc(m.size()*sizeof(double));
	
	array = m.data();
		
	cout<<"-----------\n"<< "array:\n" <<endl;
	for(int i=0; i<m.size();i++){
		
		cout<<array[i]<<" ";
		
	}
	cout<<endl;
	
	MatrixXd m2 = Map<Matrix<double,3,2,RowMajor>>(array);
	
	cout<<"-----------\n"<< "m2:\n" << m2 <<endl;
	
	
	MatrixXd m3 = MatrixXd::Ones(3,3);
	cout<<"-----------\n"<< "m3:\n" << m3 <<endl;

	//free(array);
		
}
#include <cusparse.h> 
#include <stdlib.h>

/*     Double dense matarix convert csr format. 
*      Example: eige_demo\demo\demo10
*/
void dense2csrD(double* data, int* &rowPtr, int* &colInd, double* &val, int m, int n) {

    rowPtr = (int*)malloc(sizeof(int) * (m+1));

    int* tcolInd = (int*)malloc(sizeof(int) * (m * n));
    double* tval = (double*)malloc(sizeof(double) * m * n);
    int nnv = 0;
    
    for (int i = 0; i < m; i++) {
        rowPtr[i] = nnv;
        for (int j = 0; j < n; j++) {
            if (data[j + i*m] != 0) {
                tcolInd[nnv] = j;
                tval[nnv] = data[j + i*m];
                nnv++;
            }
        }    
    }
    rowPtr[m] = nnv;
    
    colInd = (int*)malloc(sizeof(int) * nnv);
    val = (double*)malloc(sizeof(double) * nnv);

    memcpy(colInd, tcolInd, sizeof(int) * nnv);
    memcpy(val, tval, sizeof(double)*nnv);
    
    free(tcolInd);
    free(tval);

}

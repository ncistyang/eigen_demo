#include<iostream>
#include <Eigen/Dense>
#include <string.h>
#include "dense2csr.h"   


using namespace Eigen;
using namespace std;


void dense2csrD(double* data, int* &rowPtr, int* &colInd, double* &val, int m, int n) {

    rowPtr = (int*)malloc(sizeof(int) * (m+1));

    int* tcolInd = (int*)malloc(sizeof(int) * (m * n));
    double* tval = (double*)malloc(sizeof(double) * m * n);
    int nnv = 0;
    
    for (int i = 0; i < m; i++) {
        rowPtr[i] = nnv;
        for (int j = 0; j < n; j++) {
            if (data[j + i*m] != 0) {
                tcolInd[nnv] = j;
                tval[nnv] = data[j + i*m];
                nnv++;
            }
        }    
    }
    rowPtr[m] = nnv;
    
    colInd = (int*)malloc(sizeof(int) * nnv);
    val = (double*)malloc(sizeof(double) * nnv);

    memcpy(colInd, tcolInd, sizeof(int) * nnv);
    memcpy(val, tval, sizeof(double)*nnv);
    
    free(tcolInd);
    free(tval);

}

void dense2csrD(double* data, int* &rowPtr, int* &colInd, double* &val, int m, int n);

int main(){

    string sep = "\n----------------------------\n";

    int m = 5;
    int n = 5;
//     double A[16] = { 1, 7, 0, 0, 
//                   0, 2, 8, 0, 
//                   5, 0, 3, 9, 
//                   0, 6, 0, 4};
    double A[] = {0, 3, 0, 0, 0,
                  0, 0, 0, 0, 0,
                  7, 5, 0, 1, 0, 
                  0, 0, 0, 0, 0, 
                  0, 0, 14, 0, 8};

    int *csrRowPtr;
    int *csrColInd;
    double *csrVal;
    dense2csrD(&A[0], csrRowPtr, csrColInd, csrVal, m, n);
    
    int nnz = csrRowPtr[m];

    cout<< sep <<"nnz = "<<nnz<<sep;
    
    MatrixXd Amat =  Map<Matrix<double,5,5,RowMajor>>(&A[0]);
    Map<MatrixXi> Row(csrRowPtr, 1, m+1);
    Map<MatrixXi> Col(csrColInd, 1, nnz); 
    Map<MatrixXd> Val(csrVal, 1, nnz);
    
    cout<< "A: \n"<< Amat << sep;
    cout<< "csrRow: \n"<< Row << sep;
    cout<< "Col: \n"<< Col << sep;
    cout<< "Val: \n"<< Val << sep;
}



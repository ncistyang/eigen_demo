#ifndef dense2csrD_H
#define dense2csrD_H

#include <cusparse.h> 

	void dense2csrD(double* data, int* &rowPtr, int* &colInd, double* &val, int m, int n);

#endif
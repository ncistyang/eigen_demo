#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

int main(){
	//----------------//
	VectorXd vect1(5);
	
	vect1 << 1,2,3,4,5;
	
	Matrix<double, 5, 5> mat1 = vect1.asDiagonal();
	
	cout <<"--------\n"<< "mat1:\n" << mat1 << endl;
	//----------------//
	
	Matrix<float, 4, 4> mat2;
	
	mat2.setZero();
	
	mat2.diagonal() << 1,2,3,4;
	
	cout <<"--------\n"<< "mat2:\n" << mat2 << endl;
	//-----------------//
	VectorXf vect2 = VectorXf::LinSpaced(6,1,6);
	
	cout <<"--------\n"<< "vect2:\n" << vect2 << endl;
	
	Matrix<float, 6, 6> mat3 = vect2.asDiagonal();
	
	cout <<"--------\n"<< "mat3:\n" << mat3 << endl;
	
	float *array = (float *)malloc(sizeof(float)*vect2.size());
	
	array = vect2.data();
	
	cout <<"--------\n"<< "array:\n"  << endl;
	for(int i= 0;i<vect2.size();i++){
		
		cout << array[i] <<" ";
		
	}
	cout<<endl;
	
	//------------------//
	Matrix<float, 6, 12> mat4;
	mat4 << mat3, mat3;
	
	cout << "-----------\n" << "mat4: \n" << mat4 <<endl;
	
	//------------------//
	Matrix<float, 12, 6, ColMajor> mat5;
	mat5 << mat3, mat3;
	
	cout << "-----------\n" << "mat5: \n" << mat5 <<endl;
	
}
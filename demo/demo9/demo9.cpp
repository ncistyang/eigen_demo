#include <Eigen/Dense>
#include <iostream>
#include <string>

using namespace std;
using namespace Eigen;

int main(){
	
	string sep = "\n---------------------\n";
	
	Matrix3f A;
	Vector3f b;
	A << 1,2,3, 4,5,6, 7,8,10;
	b << 3,3,4;
	
	Vector3f x = A.lu().solve(b);	
	
	cout << sep <<"Here is the Matrix A: \n"<< A << sep ;
	cout << "Here is the right hand side b: \n"<< b << sep ;	 	
	cout << "The solution x is : \n" << x << sep ;
	
	Matrix3f A2;
	Matrix<float,3,2> b2;

	A2 << 1,2,3, 4,5,6, 7,8,10;
	b2.col(0) << 3,3,4;
	b2.col(1) << 3,3,4;  
	
	Matrix<float,3,2> x2 = A2.lu().solve(b2);	
	
	cout << sep <<"Here is the Matrix A2: \n"<< A2 << sep ;
	cout << "Here is the right hand side b2: \n"<< b2 << sep ;	
	cout << "The solution x2 is : \n" << x2 << sep ;
	
	
}
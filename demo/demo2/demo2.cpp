# include <iostream>
# include <Eigen/Dense>

using namespace Eigen;
using namespace std;

int main(){
	
	MatrixXf m(2,2);
	m << 1.1,2.5,3.5,4.6;
	
	MatrixXf m2 = MatrixXf::Constant(2,2,1);
	
	m2 = m2 + m;
	
	cout << m2 << endl;	
}
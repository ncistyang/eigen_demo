# eigen_demo

Eigen demo for C++.

1. Add environment variable

CPLUS_INCLUDE_PATH = D:\cantera\cantera-2.6.0\ext\eigen

2. cmd command on the g++.txt(compiler MINGW64).

##########################################################################

demo1:  MatrixXd(double type data) initialize data and assign values one by one.

demo2:  MatrixXf::Constant (MatrixXf: float type data).

demo3:  Convert int array data to matrix data with Map.

demo4:  Matrix ColMajor and RowMajor.

demo5:  Diagonal matrix initialize data and assign values. 
        VectorXd function asDiagonal LineSpaced, 
        Matrix function setZero() diagonal().
	   
demo9:  Dense Matrix lu.
demo10: Dense Matrix convert CSR format by g++ compiler . 
       
        